package ru.t1.rleonov.tm.api;

import ru.t1.rleonov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
