package ru.t1.rleonov.tm.api;

import ru.t1.rleonov.tm.model.Project;
import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}
