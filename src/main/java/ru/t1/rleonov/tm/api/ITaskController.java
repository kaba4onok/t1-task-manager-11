package ru.t1.rleonov.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
