package ru.t1.rleonov.tm.api;

import ru.t1.rleonov.tm.model.Task;
import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    void clear();

}
