package ru.t1.rleonov.tm.component;

import ru.t1.rleonov.tm.api.*;
import ru.t1.rleonov.tm.constant.TerminalArguments;
import ru.t1.rleonov.tm.constant.TerminalCommands;
import ru.t1.rleonov.tm.controller.CommandController;
import ru.t1.rleonov.tm.controller.ProjectController;
import ru.t1.rleonov.tm.controller.TaskController;
import ru.t1.rleonov.tm.repository.CommandRepository;
import ru.t1.rleonov.tm.repository.ProjectRepository;
import ru.t1.rleonov.tm.repository.TaskRepository;
import ru.t1.rleonov.tm.service.CommandService;
import ru.t1.rleonov.tm.service.ProjectService;
import ru.t1.rleonov.tm.service.TaskService;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private static void exitApplication() {
        System.exit(0);
    }

    private void processCommands() {
        System.out.println("***WELCOME TO TASK-MANAGER***");
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] args) {
        final String arg = args[0];
        processArgument(arg);
    }

    private void processCommand(final String command) {
        if (command == null) {
            commandController.showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalCommands.ABOUT:
                commandController.showAbout();
                break;
            case TerminalCommands.VERSION:
                commandController.showVersion();
                break;
            case TerminalCommands.HELP:
                commandController.showHelp();
                break;
            case TerminalCommands.INFO:
                commandController.showInfo();
                break;
            case TerminalCommands.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalCommands.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalCommands.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalCommands.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalCommands.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalCommands.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalCommands.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalCommands.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalCommands.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalCommands.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalCommands.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalCommands.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalCommands.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalCommands.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalCommands.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalCommands.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalCommands.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalCommands.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalCommands.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalCommands.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalCommands.EXIT:
                exitApplication();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    private void processArgument(final String arg) {
        switch (arg) {
            case TerminalArguments.ABOUT:
                commandController.showAbout();
                break;
            case TerminalArguments.VERSION:
                commandController.showVersion();
                break;
            case TerminalArguments.HELP:
                commandController.showHelp();
                break;
            case TerminalArguments.INFO:
                commandController.showInfo();
                break;
            case TerminalArguments.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalArguments.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void runApplication(final String[] args) {
        if (args == null || args.length == 0) {
            processCommands();
            return;
        }
        processArguments(args);
    }

}
