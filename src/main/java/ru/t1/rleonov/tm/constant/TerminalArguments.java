package ru.t1.rleonov.tm.constant;

public final class TerminalArguments {

    public final static String VERSION = "-v";

    public final static String ABOUT = "-a";

    public final static String HELP = "-h";

    public final static String INFO = "-i";

    public final static String ARGUMENTS = "-arg";

    public final static String COMMANDS = "-cmd";

}