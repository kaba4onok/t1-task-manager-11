package ru.t1.rleonov.tm.service;

import ru.t1.rleonov.tm.api.ICommandRepository;
import ru.t1.rleonov.tm.api.ICommandService;
import ru.t1.rleonov.tm.model.Command;

public final class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
